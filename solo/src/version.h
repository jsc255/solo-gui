#pragma once

extern const char* const SOLO_VERSION_TAG;
extern const char* const SOLO_VERSION;
extern const char* const SOLO_RELEASE_NAME;
extern const char* const SOLO_VERSION_FULL;
extern const bool SOLO_VERSION_IS_RELEASE;
