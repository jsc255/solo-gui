import QtQuick 2.9

import "." as SoloComponents
import "effects/" as SoloEffects

Rectangle {
    color: SoloComponents.Style.appWindowBorderColor
    height: 1

    SoloEffects.ColorTransition {
        targetObj: parent
        blackColor: SoloComponents.Style._b_appWindowBorderColor
        whiteColor: SoloComponents.Style._w_appWindowBorderColor
    }
}
