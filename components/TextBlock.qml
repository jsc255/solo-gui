import QtQuick 2.9

import "../components" as SoloComponents

TextEdit {
    color: SoloComponents.Style.defaultFontColor
    font.family: SoloComponents.Style.fontRegular.name
    selectionColor: SoloComponents.Style.textSelectionColor
    wrapMode: Text.Wrap
    readOnly: true
    selectByMouse: true
    // Workaround for https://bugreports.qt.io/browse/QTBUG-50587
    onFocusChanged: {
        if(focus === false)
            deselect()
    }
}
